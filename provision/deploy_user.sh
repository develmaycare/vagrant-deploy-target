# /bin/bash

# You can use this for the Vagrant provisioning script to create a user as the
# VM is created. The password is: d3PL0Y_t35t!

sudo useradd -c "For Deployment Testing "\
             --create-home \
             --password '$6$25b0z47U$fCbjhetAGQO8K7a0nmjOoYJy0t9lSfhgd71JDEGSxgiyvCDKEgRBCdojlySxr.0/RUMFFor4CZefvDGjPsgRt0' \
             deploy;

sudo usermod --groups admin,sudo deploy;

sudo -u deploy mkdir /home/deploy/.ssh;

