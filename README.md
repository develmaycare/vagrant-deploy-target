# Vagrant Deploy Target

This simple Vagrant set up may be used to test deployment tools and scripts.

## Setup

For deploying via SSH, you need:

1. A user account on the target.
2. Your public SSH keys in the ``authorized_keys`` file for the account used
   for deployments.

### Create SSH Keys

On your host machine, create SSH keys to be used for deployment:

    ssh-keygen -t rsa -C "app_name@example.com" -f ~/.ssh/app_name;
    chmod 600 ~/.ssh/app_name.pub;

> Note: Leave the password blank if you don't want to be annoyed by a password
prompt. You may also use ``ssh-agent`` and ``ssh-add`` to avoid re-typing
passwords.

### Launch Vagrant

Launch the vagrant instance. The `deploy` user will be created automatically.

    vagrant up;

Create the deployment user on the guest machine:

    vagrant ssh;
    sudo useradd --base-dir=/var/www --comment "App Name" --create-home app_name; # remember the password
    sudo usermod --groups admin,sudo app_name;
    sudo -u app_name mkdir /home/deploy/.ssh;
    exit;

### Copy Public Key

Copy the public key to the server and set permissions:

    cat ~/.ssh/app_name.pub | ssh app_name@192.168.2.200 "cat >> .ssh/authorized_keys";
    ssh app_name@192.168.2.200 "chmod 700 .ssh; chmod 640 .ssh/authorized_keys";

### Test the Connection

Test the connection. You may be prompted to add the instance to you known
hosts:

    ssh -i ~/.ssh/app_name app_name@192.168.2.200 "uname";

An additional step may be required for sudo to work with Ubuntu 14 or later. If
you have a user with sudo capabilities, named "sudoer", you can simply do this:

    ssh sudouer@192.168.2.200 "sudo echo 'app_name ALL=(ALL) NOPASSWD:ALL' > /etc/sudoers.d/app_name";

Or you can create the file manually. It is also possible to set tighter access as well:

    app_name ALL=(ALL) NOPASSWORD:/usr/bin/apt-get

Test again:

    ssh -i ~/.ssh/app_name app_name@example.com "sudo uname";

## Wizard

The `wizard.py` script can be used to generate a custom shell script that
executes the commands described in Setup above.

