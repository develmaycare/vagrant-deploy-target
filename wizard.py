#! /usr/bin/env python

domain_name = raw_input("Domain Name: ")

project_title = raw_input("Project Title: ")

default_project_name = project_title.lower().replace(" ", "_")
project_name = raw_input("Project Name [%s]: " % default_project_name)
if not project_name:
    project_name = default_project_name

choice = raw_input("Generate SSH Keys? [Y/n] ")
if choice.lower() == "n":
    generate_ssh_keys = False
else:
    generate_ssh_keys = True

    default_key_name = project_name + "_vagrant"
    key_name = raw_input("Key Name [%s]" % default_key_name)
    if not key_name:
        key_name = default_key_name

choice = raw_input("Create Sudoers File? [y/N] ")
if choice.lower() == "y":
    create_sudoers_file = True
else:
    create_sudoers_file = False

lines = list()
lines.append("#! /usr/bin/env bash")
lines.append("")

if generate_ssh_keys:
    lines.append("# Generate SSH keys on the local machine.")
    lines.append('ssh-keygen -t rsa -C "%s@%s" -f ~/.ssh/%s;' % (project_name, domain_name, key_name))
    lines.append('chmod 600 ~/.ssh/%s.pub;' % key_name)
    lines.append("")

lines.append("# Start the Vagrant VM.")
lines.append("vagrant up;")
lines.append("")

lines.append("# The app will live in www, but this directory is not created until Apache is installed.")
lines.append("vagrant ssh --command 'sudo mkdir /var/www';")
lines.append("")

lines.append("# Create the user on the remote machine.")
lines.append("vagrant ssh --command 'sudo useradd --base-dir=/var/www --comment \"%s\" --create-home %s'; # remember the password" % (project_title, project_name))
lines.append("vagrant ssh --command 'sudo passwd %s';" % project_name)
lines.append("vagrant ssh --command 'sudo usermod --groups admin,sudo %s';" % project_name)
lines.append("vagrant ssh --command 'sudo -u %s mkdir /var/www/%s/.ssh';" % (project_name, project_name))
lines.append("")

lines.append("# Copy local SSH key to remote authorized_keys.")
lines.append('cat ~/.ssh/%s.pub | ssh %s@192.168.2.200 "cat >> .ssh/authorized_keys";' % (project_name, project_name))
lines.append('ssh %s@192.168.2.200 "chmod 700 .ssh; chmod 640 .ssh/authorized_keys";' % project_name)
lines.append("")

if create_sudoers_file:
    lines.append("# Create the sudoers file.")
    lines.append('ssh sudouer@192.168.2.200 "sudo echo \'%s ALL=(ALL) NOPASSWD:ALL\' > /etc/sudoers.d/%s";' % (project_name, project_name))
    lines.append("")

lines.append("# Test the connection.")
lines.append('ssh -i ~/.ssh/%s %s@192.168.2.200 "sudo uname";' % (project_name, project_name))
lines.append("")

print "\n".join(lines)

